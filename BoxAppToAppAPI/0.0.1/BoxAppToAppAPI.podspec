#
# Be sure to run `pod spec lint BoxAppToAppAPI.podspec' to ensure this is a
# valid spec.
#
# Remove all comments before submitting the spec.
#
Pod::Spec.new do |s|
  s.name     = 'BoxAppToAppAPI'
  s.version  = '0.0.1'
  s.summary  = 'A short description of BoxAppToAppAPI.'
  s.license  = 'Commercial'
  s.homepage = 'http://developers.box.com/onecloud/'
  s.author   = { 'Benjamin Smith' => 'benjamin.smith@box.net' }
  s.source   = { :git => 'git@bitbucket.org:citrrus/boxapptoappapi.git', :commit => '2a34c87' }
  s.platform = :ios
  s.source_files = 'Framework/Headers/'
  s.preserve_paths = 'Framework/BoxAppToAppAPI'
  s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '$(SRCROOT)/Pods/boxapptoappapi/Framework/Headers' }

  s.ios.frameworks = 'Security'
end