Pod::Spec.new do |s|
  s.name         = "Toast"
  s.version      = "1.1"
  s.summary      = "Toast is an Objective-C category that adds Android-style toast notifications to the UIView object class."
  s.homepage     = "https://github.com/scalessec/Toast"
  s.license      = 'MIT'
  s.author       = 'Charles Scalesse'
  s.source       = { :git => "git://github.com/scalessec/Toast.git", :commit => "bde0500c24" }
  s.platform     = :ios
  s.source_files = 'Toast/classes/toast'
  s.requires_arc = false
end
