Pod::Spec.new do |s|
  s.name     = 'FacebookCitrrus'
  s.version  = '3.0.7'
  s.platform = :ios
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'The iOS SDK provides Facebook Platform support for iOS apps. ' \
               'It enables you to access the Facebook Platform APIs including the Graph API, FQL, and Dialogs.'
  s.homepage = 'http://developers.facebook.com/docs/reference/iossdk'
  s.author   = 'Facebook'

  s.source   = { :git => 'https://bitbucket.org/citrrus/facebook-ios-sdk.git' }

  s.source_files = 'src/*.{h,m}', 'src/JSON/*.{h,m}'
  s.resource     = 'src/FacebookSDKResources.bundle'
  s.library      = 'sqlite3.0'
  s.header_dir   = 'FacebookSDK'

  def s.post_install(target)
    # TODO: This should be put in a pre_install since it adds a header file that need to get symlinked and added to the project file. In the meantime, pod install just needs to be run twice.
    File.open( config.project_pods_root + 'FacebookCitrrus/src/FBSDKVersion-generated.h', "w" ) { |file| file.puts '#define FB_IOS_SDK_VERSION_STRING @"3.0.7"' }

    prefix_header = config.project_pods_root + target.prefix_header_filename
    prefix_header.open('a') do |file|
      file.puts(%{\n#define MR_ENABLE_ACTIVE_RECORD_LOGGING 0})
    end
  end
end
