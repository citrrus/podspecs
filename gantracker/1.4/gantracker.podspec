#
# Be sure to run `pod spec lint gantracker.podspec' to ensure this is a
# valid spec.
#
# Remove all comments before submitting the spec.
#
Pod::Spec.new do |s|
  s.name     = 'gantracker'
  s.version  = '1.4'
  s.license  = 'MIT'
  s.summary  = 'A short description of gantracker.'
  s.homepage = 'http://EXAMPLE/gantracker'
  s.author   = { 'mhupman' => 'mhupman@citrrus.com' }
  s.source   = { :git => 'https://bitbucket.org/citrrus/gantracker.git' }
  s.platform = :ios
  s.source_files = 'Library/GANTracker.h'
  s.preserve_paths = 'Library/libGoogleAnalytics.a'
  s.framework = 'CFNetwork'
  s.library = 'GoogleAnalytics', 'sqlite3'
  s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '$(SRCROOT)/Pods/gantracker/Library' }
end
