#
# Be sure to run `pod spec lint Box.podspec' to ensure this is a
# valid spec.
#
# Remove all comments before submitting the spec.
#

Pod::Spec.new do |s|
  s.name     = 'Box'
  s.version  = '0.0.1'
  s.summary  = 'The Box api is a framework for ios that allows you simple access to the box web api.'
  s.license  = 'Apache License, Version 2.0'
  s.homepage = 'http://developers.box.com/onecloud/'
  s.author   = { 'Box' => 'developers@box.net' }
  s.source   = { :git => 'git@bitbucket.org:citrrus/boxframework.git', :commit => '7dc09af309a33b5ca483c3e1aa519fecf9d6786e' }
  s.platform = :ios
  s.source_files = 'Box.framework/Headers/'
  s.preserve_paths = 'Box.framework'
  s.resources = 'Box.framework/Resources/BoxCoreDataStore.momd'
  s.xcconfig  =  { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/Box"' }
  s.frameworks = 'Security', 'CoreData', 'AssetsLibrary', 'Box'

end